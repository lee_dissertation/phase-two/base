# Base_img
This image will be used as a basis for all of the containers in this cloud project.
## Installation
Use [docker-compose](https://docs.docker.com/compose/) to build the application.
```bash
$ docker-compose build
```
## Usage
Include one of the following in your Dockerfile to use this image
```Dockerfile
FROM lerring/base_lib:latest
MAINTAINER Lerring
```
```Dockerfile
FROM lerring/base_api:latest
MAINTAINER Lerring
```
## Roadmap
Change python to use cpython or pypy for higher performance
