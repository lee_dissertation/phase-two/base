import threading
from .manager import Manager

"""
Class which holds a series of callables which provide access to the api
"""
class Api():

	def __init__(self, mgr):
		"""
		Params:
			:mgr: manager corresponding to the api
		"""
		self.mgr=mgr
		self.callables={}

	def runCallable(self, callableName, names, param=None):
		"""
		Runs the specified callable
		Param:
			:callableName: name of callable to run
			:names: name of subjects
			:param: optional parameter
		Returns:
			json response
		"""
		return self.callables[callableName].runAll(names,param)

	def getSubjects(self, names):
		"""
		Gets subjects held by the manager
		Param:
			:names: subjectNames
		Returns:
			list of subjects matching names
		"""
		return self.mgr.getSubjectsAsDict(names)

	def getStatus(self, names):
		"""
		Gets subjects held by the manager, formatted as status
		Param:
			:names: subjectNames
		Return:
			list of subjects matching names
		"""
		return self.mgr.getSubjectsState(names)

"""
Class representing interfaces to the system
"""
class Callable():

	def __init__(self, mgr, state, allowedStates, functionToRun, isBackgroundTask=True):
		"""
			:mgr: manager instance for api
			:state: state the subject is changing to
			:allowedStates: states subject is allowed in
			:functionToRun: function which shall by run when called
			:isBackgroundTask: whether to run task in background
		"""
		self.mgr=mgr
		self.state=state
		self.allowedStates=allowedStates
		self.functionToRun=functionToRun
		self.isBackgroundTask=isBackgroundTask

	def runAll(self, names, param=None):
		"""
		runs callable for all found subjects
		Param:
			:names: name of subjects to run
			:param: parameter of callable
		Return:
			status of all subjects once ran
		"""
		response={}
		subjects = self.mgr.getSubjects(names)
		if not subjects:
			return { 'error' : 'subjects not found!' }
		for subject in subjects:
			response[subject.name]=self.run(subject,param)
		return response

	def run(self, subject, param=None):
		"""
		Checks if subject is in allowed states, then executes the task
		Param:
			:subject: to run callable for
			:param: optional parameter
		Return:
			resulting status of subject
		"""
		if subject.state not in self.allowedStates:
			return{ "error":"Not in correct state, current state: " + subject.state }
		if self.isBackgroundTask:
			return self.executeBackgroundTask(subject,param)
		return self.executeTask(subject,param)

	def executeBackgroundTask(self, subject, param=None):
		"""
		Executes task in background
		Param:
			:subject: to run callable for
			:param: optional parameter
		Return:
			subject state
		"""
		subject.state=self.state
		args=(subject,) if not param else (subject,param)
		t=threading.Thread(target=self.functionToRun,args=args)
		t.start()
		return subject.state

	def executeTask(self,subject, param=None):
		"""
		Executes task
		Param:
			:subject: to run callable for
			:param: optional parameter
		Return:
			subject state
		"""
		subject.state=self.state
		if not param:
			self.functionToRun(subject)
		else:
			self.functionToRun(subject,param)
		return subject.state


"""
Overrides the runall class to create an instance of the subject rather than find
"""
class CallableCreate(Callable):

	def __init__(self,mgr,state,allowedStates,functionToRun):
		"""
			:mgr: manager instance for api
			:state: state the subject is changing to
			:allowedStates: states subject is allowed in
			:functionToRun: function which shall by run when called
		"""
		super().__init__(mgr,state,allowedStates,functionToRun)

	#Override
	def runAll(self, name, param=None):
		"""
		runs callable after creating the corresponding subjects
		bypasses run and only accepts one new subject per request
		Param:
			:name: name of subjects to create
			:param: parameter of callable
		Return:
			status of subject once ran
		"""
		response={}
		subject=self.mgr.create(name)
		if not subject:
			return {'error' : 'already exists!'}
		response[subject.name]=self.executeBackgroundTask(subject,param)
		return response
