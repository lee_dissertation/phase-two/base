import subprocess

class Interface():

        @staticmethod
        def subProcess(subject, cmd, state):
                try:
                        result=subprocess.run(
                                cmd,
                                check=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT
                        )
                        subject.setState(state+' complete')
                except subprocess.CalledProcessError as err:
                        processError={}
                        processError['code']=err.returncode
                        processError['cmd']=err.cmd
                        processError['output']=str(err.output).replace('"',"'")
                        subject.setState(state+' failed')

