from .subject import Subject

import threading
import os
import time
import schedule


"""
Manages "Subjects" through recognising corresponding directories matching a criteria
used for file-based apis which need monitored
"""
class Manager():

	def __init__(self,dir,criteria):
		"""
		Param:
			:dir: directory of the subjects
			:criteria: criteria to determine if folder
			in directory is subject
		"""
		self.dir=dir
		self.criteria=criteria
		self.subjects=self.genSubjects()
		t=threading.Thread(target=self.scheduleUpdate,args=())
		t.start()

	#################################
	#				#
	# Managing Updating of services #
	#				#
	#################################

	def scheduleUpdate(self):
		"""
		Schedules updating the state of subjects every 5 seconds
		"""
		schedule.every(5).seconds.do(self.updateState)
		while 1:
			schedule.run_pending()
			time.sleep(5)

	def updateSubjects(self):
		"""
		Updates the list of subjects from reading all new subjects
		"""
		self.subjects = self.subjects + self.getNewSubjects()

	def updateState(self):
		"""
		Updates the state of subjects
		"""
		self.updateSubjects()
		subjectNames = self.findSubjectNames()
		for subject in self.subjects:
			subject.updateState(subjectNames)

	#################################
	#				#
	# Managing Discovering subjects #
	#				#
	#################################

	def genSubjects(self):
		"""
		Generates instances of subjects
		Return:
			list of subjects found in directory
		"""
		#finds and generates subjects in a directory
		subjectNames=self.findSubjectNames()
		return [
			Subject(dir=self.dir,name=name)
			for name in subjectNames
		]

	def findSubjectNames(self):
		"""
		Return:
			Gets names of all subjects matching criteria in the dir
		"""
		#find directory
		if not os.path.exists(self.dir):
			raise FileNotFoundError('Could not locate dir '+self.dir)
		if os.getcwd() != self.dir:
			os.chdir(self.dir)
		#get subjects in dir matching directory
		return [
			name
			for name in os.listdir('.')
			if os.path.isdir(name)
			and self.criteria
			in os.listdir(name)
		]

	def getNewSubjects(self):
		"""
		Gets subjects which aren't already recognized by the system
		"""
		return [
			subject
			for subject in self.genSubjects()
			if subject.name not in
				[ currSubject.name for currSubject in self.subjects ]
		]


	#Interface
	def create(self,name):
		"""
		Used to create an instance of a subject,
		to be written based on what the subject is
		"""
		pass

	################################
	#			       #
	# Managing retrieving subjects #
	#			       #
	################################


	def getSubjectsAsDict(self, names=None):
		"""
		Param:
			:names: names of subjects to return
		Returns:
			the list of subjects formatted to be a dict
		"""
		#returns subjects in json friendly format
		return [
			subject.asDict()
			for subject in self.subjects
			if not names or subject.name in names
		]

	def getSubjectsState(self, names=None):
		"""
		Param:
			:names: names of subjects to return
		Returns:
			the list of subjects formatted for status
		"""
		state=[]
		for subject in self.subjects:
			if names and subject.name not in names:
				continue
			state.append(subject.asState())
		return state

	def getSubjects(self, names=None):
		"""
		Param:
			:names: names of subjects to return
		Returns:
			the list of subjects
		"""
		return [
			subject
			for subject in self.subjects
			if not names or subject.name in names
		]

