import falcon
from ..jwt_helpers import *
from ..lerring_conf import LerringConf

"""
Authentication middleware following the template
https://falcon.readthedocs.io/en/stable/user/quickstart.html
"""
class AuthMiddleware(object):

	def __init__(self, permissions, exemptRoutes=None):
		"""
		Params:
			permissions determines what nodes should be authorised to access
			exemptRoutes are what requires no authentication
		"""
		self.permissions=permissions

		conf=LerringConf('/lerring/lerring.conf')
		#handles jwts using the RS256 alg
		self.rsJwtHandler=SelfSignedJwtRS256(
			conf.getPubSignature(),
			conf.getPrivSignature()
		)
		#handles jwts using the HS256 alg
		self.hsJwtHandler=SelfSignedJwtHS256(
			conf.getUuid()
		)
		self.exemptRoutes = exemptRoutes or []

	def process_request(self, req, resp):
		"""
		Processes the request prior to reaching the api
		Checks for jwt in header
		"""
		if req.uri_template in self.exemptRoutes:
			return

		#checks for auth header
		token = req.get_header('Authorization')
		if not token:
			raise falcon.HTTPUnauthorized(
				'TOKEN_MISSING',
				'Please provide a jwt token in the Authorization header'
			)

		#checks for permissions
		payload=self.authenticate(token)
		if 'permissions' not in payload:
			raise falcon.HTTPUnauthorized(
				'TOKEN_MISSING_PERMISSIONS',
				'expected permissions in payload'
			)

		#checks if authorised
		if not any( permission in payload['permissions'] for permission in self.permissions ):
			raise falcon.HTTPUnauthorized(
				'TOKEN_INSUFFICIENT_PERMISSIONS',
				'JWT does not feature sufficient permissions for resource'
			)

	def authenticate(self, token):
		"""
		Validates that the token has been signed by the recieving nodes private key
		Param:
			token to validate
		Return:
			token payload
		Excepts:
			exceptions cuased when validating token
		"""
		try:
			alg=Jwt.getHeaders(token)['alg']
			if alg == "HS256":
				return self.hsJwtHandler.validateToken(token)
			if alg == "RS256":
				return self.rsJwtHandler.validateToken(token)
			raise Exception("invalid algorithm")
		except Exception as e:
			raise falcon.HTTPUnauthorized(
				'TOKEN_AUTH_ERROR',
				'Reason: '+str(e)
			)

"""
Authentication middleware taken from:
https://falcon.readthedocs.io/en/stable/user/quickstart.html
"""
class RequireJSON(object):

	def process_request(self, req, resp):
		"""
		Checks for application/json content type
		"""
		if not req.client_accepts_json:
			raise falcon.HTTPNotAcceptable(
				'client does not accept json'
			)

		if req.method in ('POST', 'PUT'):
			if not req.content_type or 'application/json' != req.content_type:
				raise falcon.HTTPUnsupportedMediaType(
					'expected content_type to be application/json'
				)
