import json
import falcon
from falcon_cors import CORS
from .middleware import RequireJSON

"""
Class designed for endpoints to implement further
"""
class EndPoint():

		def __init__(self,api):
			"""
			api corresponding to endpoint
			"""
			self.api=api

		@staticmethod
		def jsonResponse(message):
			"""
			Used to respond in a more human readable way
			"""
			return json.dumps(message,indent=4)+"\n"

"""
Class designed to manage middleware and access to the app
"""
class Server():

		def __init__(self,middleware):
			"""
			prepares middleware for falcon to run before reaching endpoints
			"""
			cors = CORS(allow_all_origins=True)
			self.app = falcon.API(
				middleware=[
					cors.middleware,
					RequireJSON()
				] + middleware
			)

		def getApp(self):
			"""
			Returns:
				current app instance
			"""
			return self.app


