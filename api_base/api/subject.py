"""
base class for subjects to be found in the directory, for this system this will
represent git repositories or folders containing docker-compose files
"""
class Subject():

	def __init__(self,dir,name):
		"""
		Param:
			:dir: directory corresponding to subject
			:name: name of subject
		"""
		self.dir=dir
		self.name=name
		self.state=None

	def asState(self):
		"""
		Returns:
			how to display in json as state
		"""
		return {
			'name':self.name,
			'state':self.state
		}

	def asDict(self):
		"""
		Returns:
			how to display in json in full
		"""
		return {
			'name': self.name,
			'state': self.state,
			'dir' : self.dir
		}

	def setState(self,state):
		"""
		Sets the state, although python doesn't implement private interfaces this
		allows for more logic to be structured and re-used
		"""
		self.state=state

	#interface
	def updateState(self,currSubjects):
		"""
		to be implemented specifically corresponding to the rules of state
		I.E
			if subject is missing from curSubjects, state = REMOVED
		"""
		pass
