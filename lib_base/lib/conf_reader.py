import configparser,os

"""
Wrapper class for config parser, allowing to set a default section
"""
class ConfReader():

	def __init__(self,location,defaultSection):
		self.config=configparser.ConfigParser()
		self.defaultSection=defaultSection
		self.config.read(location)

	def getProperty(self,property,section=None):
		"""
		reads the specified property from the config file
		Param:
			property - property to read
			section - section to read from, if none then use default
		Returns:
			value for given section
		"""
		if not section:
			section = self.defaultSection
		return self.config.get(section,property)

	def getSections(self):
		"""
		Gets the sections within the config
		"""
		return self.config.sections()

