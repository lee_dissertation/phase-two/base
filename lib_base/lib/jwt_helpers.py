import jwt
try:
	from .keys import KeyFactory, KeyPair
except:
	from keys import KeyFactory, KeyPair

"""
Wrapper class for pyjwt
"""
class Jwt():

	@staticmethod
	def createToken(payload, secret, headers):
		"""
		Creates a jwt token and returns it as a string
		Param:
			headers containing algorithm
			payload to send
			secret for signing
		Returns:
			a jwt token or none if there is an issue with input
		"""
		return jwt.encode(payload,secret,algorithm=headers['alg'],headers=headers).decode('utf-8')

	@staticmethod
	def getPayload(token):
		"""
		Gets payload section from jwt token
		"""
		return jwt.decode(token,verify=False)

	@staticmethod
	def getHeaders(token):
		"""
		Gets headers section from jwt token
		"""
		return jwt.get_unverified_header(token)

	@staticmethod
	def validateToken(token,secret):
		"""
		Validates a token
		Returns:
			payload if valid
		Exceptions:
			jwt.exceptions.ExpiredSignatureError
			jwt.exceptions.DecodeError
			jwt.exceptions.InvalidKeyError
			ValueError
		"""
		algorithm=Jwt.getHeaders(token)['alg']
		return jwt.decode(token,secret,algorithms=algorithm)

"""
Creating a signature is signed by private key
Verifying a signature is using public key
Disclaimer:
	only for verifying tokens created by self
"""
class SelfSignedJwtRS256(Jwt):

	def __init__(self,publicKeyPem,privateKeyPem):
		self.publicKeyPem=publicKeyPem
		self.privateKeyPem=privateKeyPem

	#Override
	def createToken(self, payload, headers):
		"""
		Creates a jwt token and returns it as a string
		Param:
			headers containing algorithm
			payload to send
			secret for signing
		Returns:
			a jwt token or none if there is an issue with input
		"""
		headers['alg']='RS256'
		return Jwt.createToken(
			payload,
			self.privateKeyPem,
			headers
		)

	#Override
	def validateToken(self, token):
		"""
		Validates a token
		Returns:
			payload if valid
		Exceptions:
			jwt.exceptions.ExpiredSignatureError
			jwt.exceptions.DecodeError
			jwt.exceptions.InvalidKeyError
			ValueError
		"""
		return Jwt.validateToken(
			token,
			self.publicKeyPem
		)

"""
Creating a jwt is signed using uuid
Verifying a signature is using uuid
Disclaimer:
	only for verifying tokens created by self
"""
class SelfSignedJwtHS256(Jwt):

	def __init__(self,uuid=None):
		self.uuid=uuid

	#Override static method
	def createToken(self, payload, headers):
		"""
		Creates a jwt token and returns it as a string
		Param:
			headers containing algorithm
			payload to send
			secret for signing
		Returns:
			a jwt token or none if there is an issue with input
		"""
		headers['alg']='HS256'
		return Jwt.createToken(
			payload,
			self.uuid,
			headers
		)

	#Override static method
	def validateToken(self, token):
		"""
		Validates a token
		Returns:
			payload if valid
		Exceptions:
			jwt.exceptions.ExpiredSignatureError
			jwt.exceptions.DecodeError
			jwt.exceptions.InvalidKeyError
			ValueError
		"""
		return Jwt.validateToken(
			token,
			self.uuid
		)
