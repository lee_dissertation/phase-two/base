from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
import os

"""
Static class managing the generation of new keys, or keys from a file
"""
class KeyFactory():

	@staticmethod
	def genRsaKey():
		"""
		Generates a RSA KeyPair
		Return:
			Generated KeyPair containing public and private key
		"""
		privateKey=rsa.generate_private_key(
			public_exponent=65537,
			key_size=4096,
			backend=default_backend()
		)
		publicKey=privateKey.public_key()
		return KeyPair(publicKey,privateKey)

	@staticmethod
	def readPublicFromFile(fileLocation):
		"""
		Reads a public key from given file
		Param:
			location of public key
		Return:
			loaded public key
		"""
		with open(fileLocation, "rb") as keyFile:
			return serialization.load_pem_public_key(
				keyFile.read(),
				backend=default_backend()
			)

	@staticmethod
	def readPrivateFromFile(fileLocation):
		"""
		Reads a private key from given file
		Param:
			location of private key
		Return:
			loaded private key
		"""
		with open(fileLocation, "rb") as keyFile:
			return serialization.load_pem_private_key(
				keyFile.read(),
				password=None,
				backend=default_backend()
			)

	@staticmethod
	def readFromFile(publicFileLocation,privateFileLocation):
		"""
		Reads a KeyPair from file
		Param:
			location of private key
			location of public key
		Return:
			loaded KeyPair
		"""
		return KeyPair(
			publicKey=KeyFactory.readPublicFromFile(publicFileLocation),
			privateKey=KeyFactory.readPrivateFromFile(privateFileLocation)
		)


"""
Class which holds intstances of a public and private key
"""
class KeyPair():

	def __init__(self,publicKey,privateKey):
		self.publicKey=publicKey
		self.privateKey=privateKey

	def writeKeys(self,publicFileLocation,privateFileLocation):
		"""
		Writes KeyPair to public and private locations
		Param:
			location of private key
			location of public key
		"""
		KeyPair.writeKey(
			KeyPair.publicAsPem(self.publicKey),
			publicFileLocation
		)

		KeyPair.writeKey(
			KeyPair.privateAsPem(self.privateKey),
			privateFileLocation
		)

	@staticmethod
	def privateAsPem(privateKey):
		"""
		Converts a private key instance into its PEM form
		Param:
			private key to be converted
		Return:
			PEM form of private key
		"""
		return privateKey.private_bytes(
			encoding=serialization.Encoding.PEM,
			format=serialization.PrivateFormat.PKCS8,
			encryption_algorithm=serialization.NoEncryption()
		)

	@staticmethod
	def publicAsPem(publicKey):
		"""
		Converts a public key instance into its PEM form
		Param:
			public key to be converted
		Return:
			PEM form of public key
		"""
		return publicKey.public_bytes(
			encoding=serialization.Encoding.PEM,
			format=serialization.PublicFormat.SubjectPublicKeyInfo
		)

	@staticmethod
	def writeKey(key,fileLocation):
		"""
		Writes a pem to a given location
		Param:
			key to be written
			location to be writtern
		"""
		os.makedirs(os.path.dirname(fileLocation), exist_ok=True)
		with open(fileLocation, 'wb') as f:
			f.write(key)
