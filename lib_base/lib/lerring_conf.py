from .conf_reader import ConfReader
from .keys import KeyPair, KeyFactory

"""
Wrapper class to handle the lerring conf format
"""
class LerringConf(ConfReader):

	def __init__(self,location):
		super().__init__(location, 'Node')

	def getType(self):
		"""
		Returns node type
		"""
		return self.getProperty('node.type')

	def getUuid(self):
		"""
		Returns node uuid
		"""
		return self.getProperty('node.uuid')

	def getDomain(self):
		"""
		Returns main domain
		"""
		return self.getProperty('router.domain')

	def getBroker(self):
		"""
		Returns broker domain
		"""
		return self.getProperty('router.broker')

	def getGitGroup(self):
		"""
		Returns base git group
		"""
		return self.getProperty('git.group')

	def getAppDir(self):
		"""
		Returns directory to locate apps
		"""
		return self.getProperty('app.dir')

	def getPubSignature(self):
		"""
		Returns public signature
		"""
		return LerringConf.getPublicPem(
			self.getProperty('key.signature.public')
		)

	def getPrivSignature(self):
		"""
		Returns private signature
		"""
		return LerringConf.getPrivatePem(
			self.getProperty('key.signature.private')
		)

	def getPubTransport(self):
		"""
		Returns public transport key
		"""
		return LerringConf.getPublicPem(
			self.getProperty('key.transport.public')
		)

	def getPrivTransport(self):
		"""
		Returns private transport key
		"""
		return LerringConf.getPrivatePem(
			self.getProperty('key.transport.private')
		)

	@staticmethod
	def getPublicPem(location):
		"""
		Reads public key and converts to pem format
		"""
		return KeyPair.publicAsPem(
			KeyFactory.readPublicFromFile(
				location
			)
		).decode('utf-8')

	@staticmethod
	def getPrivatePem(location):
		"""
		Reads private key and converts to pem format
		"""
		return KeyPair.privateAsPem(
			KeyFactory.readPrivateFromFile(
				location
			)
		).decode('utf-8')
