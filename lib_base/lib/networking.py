import socket
import os
import netifaces as nif

"""
Class of static methods creating an interface to the nodes networking
"""
class Networking():

	@staticmethod
	def getNetwork(iface='eth0',macFile='/tmp/mac'):
		"""
		Provides the user with networking details
		Param:
			interface to get networking information for
		Return:
			networking details
		"""
		ip=Networking.getIp(iface)
		return {
			'hostname':Networking.getHostname(ip),
			'mac':Networking.getMac(macFile),
			'ip':ip
		}

	@staticmethod
	def getHostname(ip):
		"""
		Gets hostname by ip
		Param:
			ip to get hostname for
		Return:
			ips hostname
		"""
		try:
			return socket.gethostbyaddr(ip)[0]
		except:
			return "N/A"

	@staticmethod
	def getMac(path='/tmp/mac'):
		"""
		Gets the nodes mac address
		Return:
			nodes mac address
		"""
		with open(path) as macFile:
			return macFile.read().strip()
		return open(path).read().strip()


	@staticmethod
	def getIp(iface='eth0'):
		"""
		Provides the ip address matching the given interface
		Param:
			interface to retrieve ip for
		Return:
			ip address for the interface
		"""
		return os.popen('ip addr show '+iface+' | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()

	@staticmethod
	def getMacFromIp(ip):
		"""
		Gets a mac address for a given ip in the network
		Param:
			ip to get hostname for
		Return:
			mac address if found else none
		"""
		for interface in nif.interfaces():
			addr = nif.ifaddresses(interface)
			try:
				interfaceMac = addr[nif.AF_LINK][0]['addr']
				interfaceIp = addr[nif.AF_INET][0]['addr']
			except (IndexError, KeyError):
				interfaceMac = None
				interfaceIp  = None
			if interfaceIp == ip:
				return interfaceMac
		return None
