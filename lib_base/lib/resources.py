import psutil
import json
import time
"""
Class of static methods creating an interface to the nodes resources
"""
class Resources():

	def __init__(self,procfsPath="/host-proc"):
		psutil.PROCFS_PATH = procfsPath

	def getCpu(self):
		"""
		Gets a nodes cpu details for each core
		Return:
			dict containing cpu info
		"""
		cpu = {}
		cpu['count'] = psutil.cpu_count()
		cpu['cores'] = []
		usageArr=psutil.cpu_percent(interval=1,percpu=True)
		freqArr=psutil.cpu_freq(percpu=True)
		tempArr=psutil.sensors_temperatures()['cpu-thermal']
		for i in range(0,cpu['count']):
			core={}
			core['num']=i
			core['temp']=self.getTemp(i,tempArr)
			core['freq']=self.getFreq(i,freqArr)
			core['usage']=usageArr[i]
			cpu['cores'].append(core)
		return cpu

	def getTemp(self,coreNum,thermArr):
		"""
		Gets the thermal information for a core
		Return:
			core's thermals
		"""
		therm = thermArr[coreNum] if len(thermArr) > coreNum else thermArr[0]
		return therm.current

	def getFreq(self, coreNum, freqArr):
		"""
		Gets the usage information for a core
		Return:
			dict containing core's usage in %
		"""
		freq = freqArr[coreNum] if len(freqArr) > coreNum else freqArr[0]
		freqDict = {}
		freqDict['current'] = round(freq.current / 1000, 2)
		freqDict['min'] = None if not freq.min else round(freq.min / 1000, 2)
		freqDict['max'] = None if not freq.max else round(freq.max / 1000, 2)
		return freqDict

	def getMemory(self):
		"""
		Gets the current memory usage in GB
		Return:
			dict containing memory usage
		"""
		memory = {}
		virtualMem = psutil.virtual_memory()
		memory['total']     = round( virtualMem.total     / (1024.0 ** 3) , 2)
		memory['available'] = round( virtualMem.available / (1024.0 ** 3) , 2)
		memory['used']      = round( virtualMem.used      / (1024.0 ** 3) , 2)
		memory['percent']   = virtualMem.percent
		return memory

	def getSwp(self):
		"""
		Gets the current swap memory usage
		Return:
			dict containing swap memory usage
		"""
		swap = psutil.swap_memory()
		swp = {}
		swp['total'] = round(swap.total / (1024.0 ** 3), 2)
		swp['free']  = round(swap.free  / (1024.0 ** 3), 2)
		swp['used']  = round(swap.used  / (1024.0 ** 3), 2)
		swp['percent'] = swap.percent
		return swp

	def getResources(self):
		"""
		Gets all of the nodes resource information, timestamped
		Return:
			dict containing resource information
		"""
		resources = {}
		resources['stamp']=time.time()
		resources['cpu']=self.getCpu()
		resources['memory']=self.getMemory()
		resources['swp']=self.getSwp()
		return resources

	def asJson(self):
		"""
		Return:
			json containing resource information
		"""
		return json.dumps(self.getResources())
