import unittest,sys,os
#Saves issues with relative paths in python / docker
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from conf_reader import ConfReader
import configparser

"""
Tests conf readers methods
Disclaimer:
	this is a wrapper class so only focusing on functionality over strong error handling
"""
class TestConfReader(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		pass

	def testReadConf(self):
		"""
		Tests reading a conf, ensuring that sections are read
		Disclaimer:
			expecing sections in file
		"""
		conf=ConfReader('/app/lib/test/data/Test.properties','Section')
		self.assertListEqual(conf.getSections(),['Section','Section2'])

	def testReadConfFail(self):
		"""
		Attempts to read a conf file that doesn't exist
		Disclaimer:
			files without sections fail as designed
		"""
		conf=ConfReader('fail','null')
		self.assertListEqual(conf.getSections(),[])

	def testPropDefaultSection(self):
		"""
		Tests reading a named property from a section
		"""
		conf=ConfReader('/app/lib/test/data/Test.properties','Section')
		self.assertEqual(conf.getProperty('test.prop'),'val')

	def testPropDefaultSectionFail(self):
		"""
		Tests reading a non existing property
		"""
		conf=ConfReader('/app/lib/test/data/Test.properties','Section')
		with self.assertRaises(configparser.NoOptionError):
			conf.getProperty('test.fail')

	def testPropWithSection(self):
		"""
		Tests reading a property with a non-default section
		"""
		conf=ConfReader('/app/lib/test/data/Test.properties','Section')
		self.assertEqual(conf.getProperty('test.prop2','Section2'),'val2')

	def testPropWithSectionFail(self):
		"""
		Tests reading a property with a section which doesn't exist
		"""
		conf=ConfReader('/app/lib/test/data/Test.properties','Section')
		with self.assertRaises(configparser.NoSectionError):
			conf.getProperty('test.fail','NonExistingSection')
