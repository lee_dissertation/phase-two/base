import unittest,sys,os
#Saves issues with relative paths in python / docker
sys.path.insert(1, os.path.join(sys.path[0], '..'))

from keys import KeyFactory, KeyPair
from jwt_helpers import Jwt, SelfSignedJwtRS256, SelfSignedJwtHS256
#from cryptography.hazmat.primitives.asymmetric import rsa

"""
Tests the jwt class
Disclaimer:
	expects KeyFactory and KeyPair to be functional
"""
class JwtTest(unittest.TestCase):

	def testGenerateRSA256(self):
		"""
		Tests a key is successfully generated using the RSA256 algorithm
		Notes:
			create token derives alg from headers
		"""
		keyPair=KeyFactory.genRsaKey()
		headers={
			'alg' : 'RS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		self.assertIsInstance(
			Jwt.createToken(
				payload=payload,
				secret=KeyPair.privateAsPem(keyPair.privateKey),
				headers=headers
			),
			str
		)

	def testGenerateHS256(self):
		"""
		Tests a key is successfully generated using the HS256 algorithm
		Notes:
			create token derives alg from headers
		"""
		headers={
			'alg' : 'HS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		self.assertIsInstance(
			Jwt.createToken(
				payload=payload,
				secret='test',
				headers=headers
			),
			str
		)

	def testDecodeRSA256(self):
		"""
		Tests decoding an rsa token encoded with a pem private key with its public key
		"""
		keyPair=KeyFactory.genRsaKey()
		headers={
			'alg' : 'RS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		token=Jwt.createToken(
			payload=payload,
			secret=KeyPair.privateAsPem(keyPair.privateKey),
			headers=headers
		)
		self.assertIsInstance(
			token,
			str
		)
		self.assertDictEqual(
			Jwt.getPayload(token),
			payload
		)
		self.assertDictEqual(
			Jwt.getHeaders(token),
			headers
		)
		self.assertTrue(
			Jwt.validateToken(
				token,
				KeyPair.publicAsPem(keyPair.publicKey)
			)
		)

	def testDecodeHS256(self):
		"""
		Tests a key is successfully decoded using the HS256 algorithm
		Notes:
			create token derives alg from headers
		"""
		headers={
			'alg' : 'HS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		secret='test'
		token=Jwt.createToken(
			payload=payload,
			secret=secret,
			headers=headers
		)
		self.assertIsInstance(
			token,
			str
		)
		self.assertDictEqual(
			Jwt.getPayload(token),
			payload
		)
		self.assertDictEqual(
			Jwt.getHeaders(token),
			headers
		)
		self.assertTrue(
			Jwt.validateToken(token,secret)
		)

"""
Tests the rapper class which uses uuid for self signed JWT tokens
"""
class TestSelfSignedJwtHS256(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		cls.secret='test_uuid'
		cls.jwtBuilder=SelfSignedJwtHS256(cls.secret)

	def testSelfSignedHS(self):
		"""
		Tests generating and validating a RS256 token
		"""
		headers={
			'alg' : 'HS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		token=self.jwtBuilder.createToken(
			payload=payload,
			headers=headers
		)
		self.assertIsInstance(
			token,
			str
		)
		self.assertDictEqual(
			Jwt.getPayload(token),
			payload
		)
		self.assertDictEqual(
			Jwt.getHeaders(token),
			headers
		)
		self.assertTrue(
			self.jwtBuilder.validateToken(token)
		)

"""
Tests the rapper class which manages keys for self signed JWT tokens
"""
class TestSelfSignedJwtRS256(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		keyPair=KeyFactory.readFromFile(
			publicFileLocation='/app/lib/test/data/pub.pem',
			privateFileLocation='/app/lib/test/data/priv.pem'
		)
		cls.jwtBuilder=SelfSignedJwtRS256(
			publicKeyPem=KeyPair.publicAsPem(keyPair.publicKey),
			privateKeyPem=KeyPair.privateAsPem(keyPair.privateKey)
		)

	def testSelfSignedRSA(self):
		"""
		Tests generating and validating a RS256 token
		"""
		headers={
			'alg' : 'HS256',
			'typ' : 'JWT'
		}
		payload={
			'name' : 'test'
		}
		secret='test'
		token=self.jwtBuilder.createToken(
			payload=payload,
			headers=headers
		)
		self.assertIsInstance(
			token,
			str
		)
		self.assertDictEqual(
			Jwt.getPayload(token),
			payload
		)
		self.assertDictEqual(
			Jwt.getHeaders(token),
			headers
		)
		self.assertTrue(
			self.jwtBuilder.validateToken(token)
		)
