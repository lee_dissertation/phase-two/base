import unittest,sys,os
#issues with relative paths in python / docker
sys.path.insert(1, os.path.join(sys.path[0], '..'))

from keys import KeyFactory, KeyPair
from cryptography.hazmat.primitives.asymmetric import rsa

"""
Tests key factory methods
Disclaimer:
	files are fullpath due to docker not understanding relative paths during build process
"""
class KeyFactoryTest(unittest.TestCase):

	def testGenKeys(self):
		"""
		Tests generating a key pair and confirming its resulting object
		"""
		self.assertIsInstance(
			KeyFactory.genRsaKey(),
			KeyPair
		)

	def testReadPublicKey(self):
		"""
		Tests reading a public key from a file
		"""
		self.assertIsInstance(
			KeyFactory.readPublicFromFile('/app/lib/test/data/pub.pem'),
			rsa.RSAPublicKey
		)

	def testReadPrivateKey(self):
		"""
		Tests reading a private key from a file
		"""
		self.assertIsInstance(
			KeyFactory.readPrivateFromFile('/app/lib/test/data/priv.pem'),
			rsa.RSAPrivateKey
		)

	def testReadKeys(self):
		"""
		Tests reading a public and private key into a keypair instance
		"""
		self.assertIsInstance(
			KeyFactory.readFromFile(
				'/app/lib/test/data/pub.pem',
				'/app/lib/test/data/priv.pem'
			),
			KeyPair
		)

"""
Tests key pair methods
Disclaimer:
	files are fullpath due to docker not understanding relative paths during build process
"""
class KeyPairTest(unittest.TestCase):

	def testPublicAsPem(self):
		publicKey=KeyFactory.readPublicFromFile('/app/lib/test/data/pub.pem')
		pem="""-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA12uy9PRdjzw2qJDX1Ho/
UmKxIi92421LHpWfGGAfz9EFtgw7cOoikVd7zrES/D8Sh6Dwf4D/CO4ZdtECXC55
HtrzttnZr0J+3lLEniYX78s+s3DIOxfp2wYGqmBvFggJNaDHeJDseTJJ26IZU0Y/
8KLlpzSwOWVZ5IK03J0aYHhfXYjUTU+inZo0jin1nyu/+MRCoLjm6szkH1gD+tOy
J1yqpqjXX6XddbQzMADGQ9nTIgCk77x+tkfRlDa4CU8Ft6/XRrHw0k5JuzbQckIC
vyi6TNcZllyRBqHbKMktPXC+wiFTYf3iH4p/nf5zopR0FMOB9ymqfbS+SlXGbOjx
bMneJbho+jWUV+fmp5vDhVxBjnpOJOZsCsi6sn90FCHy7IX5iZZTnAhhjHqqO6/C
052RyaJtU1izFvlByIyikZ7j8BZv69gZZwIgkOshU2Rh1fZJAao1xDmfqOWAXSdi
+hdrot4tWQYkXCSgwGdXpMfM1ju2fy6y7coY+tOKQrswfYBoB+rQuuViDFsrj+qo
veqZMa9NqCrZh2Jkw1/cDB+pW11G9HlWrOrWZsXEyHqRnPpCIvHyoGi+YL5SzLFI
t9UJ16R6Oj8qJBuXRLSkdBdpjGH5aTPD20I9sgG4lXPwGhj00a2MbsBY/RycaIvk
03/xHzGqKNv99bLu9JW8h3sCAwEAAQ==
-----END PUBLIC KEY-----
"""
		self.assertEqual(
			KeyPair.publicAsPem(publicKey).decode('utf-8'),
			pem
		)

	def testPrivateAsPem(self):
		privateKey=KeyFactory.readPrivateFromFile('/app/lib/test/data/priv.pem')
		pem="""-----BEGIN PRIVATE KEY-----
MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDXa7L09F2PPDao
kNfUej9SYrEiL3bjbUselZ8YYB/P0QW2DDtw6iKRV3vOsRL8PxKHoPB/gP8I7hl2
0QJcLnke2vO22dmvQn7eUsSeJhfvyz6zcMg7F+nbBgaqYG8WCAk1oMd4kOx5Mknb
ohlTRj/wouWnNLA5ZVnkgrTcnRpgeF9diNRNT6KdmjSOKfWfK7/4xEKguObqzOQf
WAP607InXKqmqNdfpd11tDMwAMZD2dMiAKTvvH62R9GUNrgJTwW3r9dGsfDSTkm7
NtByQgK/KLpM1xmWXJEGodsoyS09cL7CIVNh/eIfin+d/nOilHQUw4H3Kap9tL5K
VcZs6PFsyd4luGj6NZRX5+anm8OFXEGOek4k5mwKyLqyf3QUIfLshfmJllOcCGGM
eqo7r8LTnZHJom1TWLMW+UHIjKKRnuPwFm/r2BlnAiCQ6yFTZGHV9kkBqjXEOZ+o
5YBdJ2L6F2ui3i1ZBiRcJKDAZ1ekx8zWO7Z/LrLtyhj604pCuzB9gGgH6tC65WIM
WyuP6qi96pkxr02oKtmHYmTDX9wMH6lbXUb0eVas6tZmxcTIepGc+kIi8fKgaL5g
vlLMsUi31QnXpHo6PyokG5dEtKR0F2mMYflpM8PbQj2yAbiVc/AaGPTRrYxuwFj9
HJxoi+TTf/EfMaoo2/31su70lbyHewIDAQABAoICAFUsR/2CANv5OsM7QZrJGvBA
BTk2+xLAy8qz7ct9dhZ81n08KORkLna42ELI0HDbnz/w6iX/otatDXRk/0HjPhBt
EZ9iUydwaykMIKFG/n/ZJFleZky3D8eBgnEq38EePEXv4+5Pnq/sqjckyE6cn/tM
7PBHbMXExN4Mr+XqV9t8vHgG+an3qpwVtGHhRTRB4A1Dy/QwJ1z+1dNuwWQS/4WA
phhu+Xjf48f+D58TL1D0dv4isxP2u0Lm8n5fhDRqAiC2ba+N+2Grgot7CL+kSeG7
AK4Q/hTtVIoCxNxDbpe0VDf9ZTKBZz6BTCPzz1SgzaTICELpPnelVvedIm89Sva2
sjpJBcPXjRcXdbr6OPz3XR/ivWW5fbrwDZE4SrbQve935gDMslUXxj6JD6/auGGB
MJqbg3mpku/aT/Zzwd2LjyJAmgffb2dM1bSlIJ1YxuEJ7Bztvjbklsg1XIDBnAfU
/idpAA6TJOw1NnWKeRxtJ0DZpPhPMEgHlwFxQSP5L58kwr40LnrfENuSKez45Eao
UnDgRf8LcNQhu92anbkPFvFXdQDIw/8A/34/FWfsN3cSS9ZbKU9ImsOyTcMNst+F
K1Gkd24pPfAabrO9WnJ1splxLpi6cByTGe2Y2hZnfO9OlDuPhmw1st7PJYxoNpAt
oIBbOzbqk3Nlc2XFCDfBAoIBAQDxzxMF5K4lKcLTPUSvjvriHQDqcUn1Fb3ny8aP
pIa38MqTpAZ2prvhMjb5OH2H+l5k6DK0t4nLD/sjahq5n3dbZGP1FywZQ2lTfomN
FwY/Bxnm5HDMtxRpY/AuKqbA2Lh+XddDypxFOOS7wAem9qjYUCFINAeI5KmZzDHL
AASIong1yVsEEAMT2YRBAKJBRFV8k/HCXmSTbUlvibZNhXDpakDgZWQkyaZbCvjh
uoo1mox3c50Y8VMv4pWZGk5bRsd0dYJ2lq/NxNohPYiihms2Da7EW0tt8zpBzYFG
jMEKTBopwUbPURnd+AS37f60n4y3O0c8lwMsEfLGa0AJ5TZNAoIBAQDkECt7lfQL
MNHk4Da92bxYiBFRSt6rHkxtI7Sh13cDNrChG/dwa36Z1ZChybOepueSbWez9ARi
wPh0O+PZQL0Gl/3JpxHZovIZ1hlwUyYWiGZgWsbUN23KP9zz/Q1H05aNYFhgOEx9
Y0rkEdr3Fej73At80Wfg1yBOqNpW/3tZU4e842h3WADzo+5sySsf2yAlDwnWa5h2
dzt5I50DNZyWRq/N1MHoihqCNxWzKMLgg/RbQnaafYXzmiaKc8wS7QWPBmT9HNEE
IFsL1Fa0ooCKXlMG5OoSjSQrw+IkIpb8TMFNdQrzE+8zy7RSni0BNbPQNkxXcLSb
gc5bBQ58XqjnAoIBAQCw47Cg0QbrvL7+1IUYgFnbayguWK2nkj4YASPCFLNQf99R
Xd40kghC1v8MFV0PSgIfgxUEyncMiyZGY+ok/BjOTIQq2MazvHKiV6C8C6nQFZvH
Ijm6J/vQlQEwZJcT3/YmyvMcbm0e3l3FUtbGNw6RHx0ZuqYAaHYBQjTysZKfeWrC
vgfM0wHNVYPAIXyQjjKTLY7fhzm/92SPKYio8QMGXWoUPVei6bi+RSqHxnQwflxf
Tb4QbwlRsEmLdIj5kK977DKCGf2FY4nGY0M+0WJAaoX8smRdjF6DoJf/Shw1MjK5
dOAd7NAlZrfc7Uc0Trsbl0uncTkq1f5pVC/GjVzJAoIBAQCv0bd0A1qs9eoapLAP
FLkbF2siXLHlAeqxiQFMc6uQkq0Ax2qvBHAJcJeb4r/S46FLRYTZOpnW+CWSlklF
x57yYmKI/3/HZNN4D5J1AeUvIeCrD9CQ8lp1vV49yyxd30UFoO0A4OY3FEE8KnfA
XOfMowtIIrgOokqJ2arZvwP9DWRrvnVe7D1vl0XMIBpa6LeTWlCfOTvEuI7lCQuW
2dHqGHyYFhmXiEB/erAD+ryiEZhkcoy09Wg7MO/ddQAy5v+OD8PzTi/lGnyWVhCy
GUU3ZxhjDD9YJe9ovg6tBO2ZfCLZ8A2M8CVMrRJLTx4q/JevQO0BzCWwbRYFHZTZ
VLCpAoIBAQDCKVsA6zlhTPydUlJ5tjsLZiyTvN2gs3NvcTqbR0uqqxqLdih+wEL1
OBLghtvCNC2JRleWB1BmPSsR8EoDmn86EFNDCdAl62gFSkRXas57SuloesP20YR9
pabM8RURZBMnFbRliHE4Ue93uECko863q3OJrcJ4x7XAcjAXVZ12jFDQl9i4YTdr
PTME1LPKYGltkW/RTULFbDKrEWFDwM6riIWrdi6l/p50KEZHLETOflHt1ijOkLcN
m4fdOzEh2N+bsJM8b7NQ9HteKQaLbeDOlqkB039bnTNYp5h+GHFPRLqPlOYSR3GN
wTRQ5gabECmUjJ7HwjQFUq3Bme2L9/m9
-----END PRIVATE KEY-----
"""
		self.assertEqual(
			KeyPair.privateAsPem(privateKey).decode('utf-8'),
			pem
		)

