import unittest,sys,os
#Saves issues with relative paths in python / docker
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from networking import Networking

"""
Testing networking static class
	Disclaimer:
		Presumes the tests have passed for the methods used in other tests
		Test cannot check real details without being inside a container using network_mode = host
"""
class NetworkTest(unittest.TestCase):

	def setUp(self):
		"""
		Unit test causes warnings for using subprocess
		"""
		if not sys.warnoptions:
			import warnings
			warnings.simplefilter("ignore")

	def testGetMac(self):
		"""
		Tests for getting a mac address
		Disclaimer:
			Using regex as mac is docker assigned unless network mode host
		"""
		self.assertRegex(
			Networking.getMac('/app/lib/test/data/mac_addr'),
			'([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})'
		)

	def testGetIp(self):
		"""
		Tests for getting an ip due
		Disclaimer:
			DHCP this is only looking at regex match
		"""
		self.assertRegex(
			Networking.getIp(),
			'((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4}'
		)

	def testGetMacFromIp(self):
		"""
		Tests getting a mac address for a given ip
		"""
		self.assertRegex(
			Networking.getMacFromIp(
				Networking.getIp()
			),
			'([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})'
		)

	def testGetHostname(self):
		"""
		Tests getting a hostname for given interface
		"""
		self.assertIsInstance(
			Networking.getHostname(
				Networking.getIp()
			),
			str
		)

	def testGetNetworkDetail(self):
		"""
		Tests whether produced dict features expected keys
		"""
		details=Networking.getNetwork(macFile='/app/lib/test/data/mac_addr')
		self.assertIn('hostname',details)
		self.assertIn('mac',details)
		self.assertIn('ip',details)
