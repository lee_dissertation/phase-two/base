import unittest,sys,os
#Saves issues with relative paths in python / docker
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from resources import Resources

"""
Testing resources static class
Disclaimer:
	expects networking class to be tested
"""
class ResourceTest(unittest.TestCase):

	@classmethod
	def setUp(cls):
		cls.resources=Resources('/proc')

	def testGetSwp(self):
		"""
		Asserts expected keys are provided by getSwp()
		"""
		swp=self.resources.getSwp()
		self.assertIn('total',swp)
		self.assertIn('free',swp)
		self.assertIn('used',swp)
		self.assertIn('percent',swp)

	def testGetMemory(self):
		"""
		Asserts expected keys are provided by getMemory()
		"""
		mem=self.resources.getMemory()
		self.assertIn('total',mem)
		self.assertIn('available',mem)
		self.assertIn('used',mem)
		self.assertIn('percent',mem)

	def testGetCpu(self):
		"""
		Asserts expected keys are provided by getCpu()
		"""
		cpu=self.resources.getCpu()
		self.assertIn('count',cpu)
		self.assertIn('cores',cpu)

